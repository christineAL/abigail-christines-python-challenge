# Abigail & Christine's Python Challenge

## To Create the Database and Tables for the API Data
* copy and paste the following command into your terminal:
  * **mysql -h mysql.grads.al-labs.co.uk -u _(your username)_ -psecret <CreateDB.sql**

## To collect data
* Run **python getweather** in terminal 

## To execute required data display commands
* copy and paste the following command into your terminal:
  * **mysql -h mysql.grads.al-labs.co.uk -u _(your username)_ -psecret <DataDisplay.sql**
