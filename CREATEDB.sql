DROP SCHEMA PCAC;
CREATE DATABASE PCAC;
use PCAC;
 CREATE TABLE CITY (
 WOEID int auto_increment primary key,
 City varchar(30) not null,
 Latt_Long varchar(30) not null,
 Timezone varchar(30) not null);

CREATE TABLE Tokyo (
WOEID int(30),
Date_Time datetime,
Weather_State varchar(50) not null,
Wind_Direction varchar(50) not null,
Min_Temp float(30) not null,
Max_Temp float(30) not null,
Temperature int(30) not null,
Air_Pressure float(30) not null,
Humidity float(30) not null,
Predictability float(30) not null,
Sun_Rise datetime not null,
Sun_Set datetime  not null,
FOREIGN KEY(WOEID) REFERENCES CITY(WOEID));


CREATE TABLE London AS SELECT * FROM Tokyo;
CREATE TABLE `New York` AS SELECT * FROM Tokyo;
CREATE TABLE Sydney AS SELECT * FROM Tokyo;
CREATE TABLE `Cape Town` AS SELECT * FROM Tokyo;
CREATE TABLE `Rio de Janeiro` AS SELECT * FROM Tokyo;
