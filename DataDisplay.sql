-- Get a list of Cities in your database that you have data for
use PCAC;
SELECT City FROM CITY;

-- Get the Woeid of a city, and/or list all cities and their woeid
SELECT City, WOEID FROM CITY;

-- Specify a date and a city and in return be told:
-- The average min and max temperatures for the date according to the service
select date_time "Date-Time", avg(Min_Temp) "Average Min Temp" ,avg(Max_Temp) "Average Max Temp" from London
where date_time like '2019-11-21%';

-- The average predictability the service gave for the date
select date_time "Date-Time", avg(Predictability) "Average Predictability"
from London
where date_time like '2019-11-21%';

-- The actual min and max temperatures calculated from the actual temperatures you recorded
select date_time "Date-Time", min(Temperature) "Actual Min Temp", max(Temperature) "Actual Max Temp" from London
where date_time like '2019-11-21%';

-- The order of the weather state as it occurred each hour of that date between 9am and 6pm
select date_time "Date-Time", Weather_State "Weather State"
from London
where date_time BETWEEN '2019-11-21 10:00:00' and '2019-11-21 18:00:00'
order by date_time;

select date_time "Date Time", Weather_State "Weather State"
from London
where date_time like '%59:%' or date_time like '%00:%';

SELECT  Weather_State, date_time, FLOOR(UNIX_TIMESTAMP(date_time)/(59* 60)) AS timekey
From London
Group by timekey;

SELECT  Weather_State, date_time, FLOOR(UNIX_TIMESTAMP(date_time)/(59* 60)) AS timekey
From Tokyo
Group by timekey;

SELECT  Weather_State, date_time, FLOOR(UNIX_TIMESTAMP(date_time)/(59* 60)) AS timekey
From Sydney
Group by timekey;

SELECT  Weather_State, date_time, FLOOR(UNIX_TIMESTAMP(date_time)/(59* 60)) AS timekey
From `Rio de Janeiro`
Group by timekey;

SELECT  Weather_State, date_time, FLOOR(UNIX_TIMESTAMP(date_time)/(59* 60)) AS timekey
From `New York`
Group by timekey;

SELECT  Weather_State, date_time, FLOOR(UNIX_TIMESTAMP(date_time)/(59* 60)) AS timekey
From `Cape Town`
Group by timekey
